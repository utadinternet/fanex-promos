import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.css";
import { Route } from "react-router-dom";
import { authenticate } from "./API";
var base64 = require("base-64");
var encodedData = base64.encode();

export class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: "",
      pass: "",
      authkey: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }
  handleSubmit = async event => {
    const submitState = {
      username: this.state.user,
      password: base64.encode(this.state.pass)
    };
    //alert("Was submitted: " + submitState);
    event.preventDefault();
    try {
      await authenticate(submitState);
      let authkey = Promise.resolve(authenticate(submitState));
      authkey.then(value => {
        this.setState({
          authkey: value.description
        });
        sessionStorage.setItem("token", value.description);
        window.location = "/promos";
      });
    } catch (e) {
      alert(e.message);
    }
  };

  render() {
    return (
      <div className="page-one-bg">
        <div className="container">
          <div className="row" style={{ padding: 20 }}>
            <div className="card card-signin w-50 text-center bg-light mx-auto">
              <div className="card-body">
                <img
                  className="app-logo"
                  src="https://assetbank.ath.utk.edu/asset-bank/images/standard/t-logo.png"
                  alt="logo"
                />
                <h5 className="card-title text-center">
                  Fan Experience Promotions Editor
                </h5>
                <form className="form-signin">
                  <div className="row justify-content-center">
                    <div className="form-label-group col-md-7 ">
                      <label for="Username">Username</label>
                      <input
                        type="email"
                        className="form-control"
                        name="user"
                        // value={this.state.user}
                        onChange={event => this.handleChange(event)}
                      />
                    </div>
                  </div>
                  <div className="row justify-content-center">
                    <div className="form-label-group col-md-7">
                      <label for="Password">Password</label>
                      <input
                        type="password"
                        className="form-control"
                        name="pass"
                        // value={this.state.pass}
                        onChange={event => this.handleChange(event)}
                      />
                    </div>
                  </div>
                </form>
                <button
                  type="button"
                  className="btn btn-success"
                  onClick={this.handleSubmit}
                >
                  Login
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
