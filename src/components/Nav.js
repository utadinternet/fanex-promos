import React, { Component } from "react";
import { Link } from "react-router-dom";

const Header = () => (
  <header>
    <nav>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/promos">Promotions</Link>
        </li>
      </ul>
    </nav>
  </header>
);
