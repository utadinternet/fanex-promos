import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.css";
import { EditorState } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import moment from "moment";
import { getAllPromos, authenticate } from "./API.js";
import EditModal from "../Modal.js";
import { Route, Switch, Link, BrowserRouter } from "react-router-dom";
import { LoginForm } from "./LoginForm";

moment.locale("en");

const Main = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={LoginForm} />
      <Route path="/promos" component={PromoPage} />
    </Switch>
  </BrowserRouter>
);

class Card extends React.Component {
  render() {
    return <div>[this.props.value]</div>;
  }
}
class CardContainer extends React.Component {
  render() {
    var arr = [];
    var elements = [];
    for (var i = 0; i < arr.length; i++) {
      elements.push(<Card value={arr[i]} />);
    }
    return <div>{elements}</div>;
  }
}

const Header = () => (
  <header>
    <nav>
      <ul>
        <li>
          <Link to="src/components/LoginForm.js">Home</Link>
        </li>
        <li>
          <Link to="/promos">Promotions</Link>
        </li>
      </ul>
    </nav>
  </header>
);
class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <Main />, <Header />;
  }
}

export default App;

class PromoPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      promosList: null,
      editorState: EditorState.createEmpty(),

      token: this.props.authkey,
      sport: "",
      opponent: "",
      date: null,
      time: "",
      text: "",
      promodata: undefined
    };
    this.handler = this.handler.bind(this);
    this.createNew = this.createNew.bind(this);
  }
  handler = val => {
    this.setState(
      {
        sport: val.sport,
        opponent: val.opponent,
        date: val.date,
        time: val.time,
        promodata: val.id,
        text: val.promotions
      },
      () => {
        this.forceUpdate();
      }
    );
  };

  createNew() {
    this.setState({
      sport: "",
      opponent: "",
      date: null,
      time: "",
      promodata: undefined
    });
  }

  async componentDidMount() {
    try {
      const { events } = await getAllPromos();

      this.setState({
        promosList: events
      });
      this.forceUpdate();
    } catch (err) {}
  }
  onEditorStateChange = editorState => {
    this.setState({
      editorState
    });
  };

  logoff() {
    sessionStorage.removeItem("token");
    window.location.replace("/");
  }

  render() {
    const { promosList } = this.state;
    let promoCards;

    if (promosList != null) {
      promoCards = promosList.map((promo, index) => {
        return <PromoCard promo={promo} key={index} action={this.handler} />;
      });
    } else if (sessionStorage.getItem("token") == null) {
      this.props.history.push("/");
    } else if (
      sessionStorage.getItem("token") == "Username or password was incorrect"
    ) {
      this.props.history.push("/");
    }

    return (
      <div className="App">
        <header className="promo-header">
          <img
            className="promo-logo"
            src="https://assetbank.ath.utk.edu/asset-bank/images/standard/t-logo.png"
            alt="logo"
          />
          <h1 className="promo-title text-center">FAN EXPERIENCE PROMOTIONS</h1>
        </header>
        <div className="alert alert-warning" role="alert">
          <button
            type="button"
            className="btn btn-success"
            onClick={this.createNew}
          >
            Create New Event
          </button>
          <a
            href="https://utsports.com/sports/2017/9/27/fan-experience.aspx"
            type="button"
            className="btn btn-warning"
            target="_blank"
          >
            View Site
          </a>
          <button
            type="button"
            className="btn btn-danger signbtn"
            onClick={this.logoff}
          >
            Sign Out
          </button>
        </div>
        <div className="div-wrap">
          <div className="promo-div">{promoCards}</div>
          <div className="edit-div">
            <div className="card w-75 text-center bg-light mx-auto">
              <div className="card-body">
                <h5 className="card-title text-center">
                  Fan Experience Promotions Editor
                </h5>
                <EditModal
                  handler={this.handler}
                  sport={this.state.sport}
                  opponent={this.state.opponent}
                  date={this.state.date}
                  time={this.state.time}
                  promodata={this.state.promodata}
                  text={this.state.text}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export { PromoPage };

class PromoCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sport: "",
      opponent: "",
      date: null,
      time: "",
      promodata: "",
      text: ""
    };
  }

  setEdit = () => {
    const { promo } = this.props;
    this.setState({ sport: promo.sport });
    this.setState({ opponent: promo.opponent });
    this.setState({ date: promo.date });
    this.setState({ time: promo.time });
    this.setState({ text: promo.promotions });
    this.setState({ promodata: promo.id }, () => {
      this.props.action(promo);
    });
  };
  render() {
    const { promo } = this.props;

    return (
      <div>
        <div className="card w-75 text-center bg-light mx-auto">
          <div className="card-body">
            <h1>{promo.sport} </h1>
            <h3>{promo.opponent}</h3>
            <p>
              {promo.date} {promo.time}
            </p>
            <p>{promo.promotions}</p>
            <button
              type="button"
              className="btn btn-warning btn-lg btn-block"
              opponent={promo.opponent}
              onClick={this.setEdit}
            >
              Edit event
            </button>
          </div>
        </div>
      </div>
    );
  }
}
