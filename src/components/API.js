const token = sessionStorage.getItem("token");

export async function getAllPromos() {
  return fetch(
    "https://utadinternet.com/code/promos/api/upcoming_promos_dev.php",
    {
      method: "GET",
      headers: {
        Auth: token
      }
    }
  ).then(response => response.json());
}

export async function sendNewPromo(data) {
  return fetch(
    "https://utadinternet.com/code/promos/api/insert_promos_dev.php",
    {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Auth: token
      }
    }
  ).then(response => response.json());
}

export async function sendPromoUpdate(data) {
  return fetch(
    "https://utadinternet.com//code/promos/api/update_promos_dev.php",
    {
      method: "PUT",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Auth: token
      }
    }
  ).then(response => response.json());
}

export async function deletePromo(id) {
  return fetch(
    "https://utadinternet.com/code/promos/api/delete_promos_dev.php",
    {
      method: "DELETE",
      body: JSON.stringify(id),
      headers: {
        "Content-Type": "application/json",
        Auth: token
      }
    }
  ).then(response => response.json());
}

export async function authenticate(data) {
  return fetch("http://utadinternet.com/code/promos/api/auth_dev.php", {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json"
    }
  }).then(response => response.json());
}
