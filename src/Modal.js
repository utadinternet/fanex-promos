import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.css";
import { EditorState, convertFromRaw, ContentState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import "react-dates/initialize";
import { SingleDatePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import moment from "moment";
import TimePicker from "react-time-picker";
import { sendPromoUpdate, deletePromo } from "./components/API.js";
import { sendNewPromo } from "./components/API.js";
import FileBase64 from "react-file-base64";
var convertTime = require("convert-time");

moment.locale("en");
const content = {
  entityMap: {},
  blocks: [
    {
      key: "637gr",
      text: "Initialized from content state.",
      type: "unstyled",
      depth: 0,
      inlineStyleRanges: [],
      entityRanges: [],
      data: {}
    }
  ]
};

export default class EditModal extends React.Component {
  constructor(props) {
    super(props);
    const contentState = convertFromRaw(content);

    this.state = {
      editorState: EditorState.createWithContent(
        ContentState.createFromText(this.props.text)
      ),
      contentState,
      sport: this.props.sport,
      opponent: this.props.opponent,
      date: moment(),
      time: "",
      id: this.props.promodata,
      files: [],
      text: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillReceiveProps() {
    if (this.props.date !== null) {
      let newDate = moment(this.props.date);
      let newTime = convertTime(this.props.time);
      this.setState({ date: newDate, time: newTime });
    }

    this.setState(
      {
        sport: this.props.sport,
        opponent: this.props.opponent,
        id: this.props.promodata
      },
      () => {
        var text = String(this.props.text);
        const contentState = ContentState.createFromText(text);
        this.state.editorState = EditorState.createWithContent(contentState);
      }
    );
  }
  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value }, () => {});
  }

  handleSubmit(event) {
    const submitState = {
      texteditor: this.state.contentState,
      sport: this.state.sport,
      opponent: this.state.opponent,
      date: this.state.date,
      time: this.state.time,
      id: this.state.id,
      image: this.state.files
    };
    alert(" Please refresh to see changes.");
    event.preventDefault();
    if (this.state.id == undefined) {
      sendNewPromo(submitState);
    } else {
      sendPromoUpdate(submitState);
    }
  }

  deleteItem() {
    deletePromo(this.state);
    alert("Event was deleted ");
  }
  onContentStateChange: Function = contentState => {
    this.setState({
      contentState
    });
  };

  onEditorStateChange = editorState => {
    this.setState({
      editorState
    });
  };
  componentDidMount() {}
  getFiles(files) {
    this.setState({ files: files });
  }
  render() {
    const { editorState } = this.state;
    const { contentState } = this.state;

    return (
      <div className="card w-75 text-center bg-light mx-auto">
        <div className="card-body">
          <form className="justify-content-center">
            <div className=" row">
              <div className="form-group col-lg-7">
                <label for="sport">Sport:</label>
                <select
                  className="custom-select"
                  value={this.state.sport}
                  name="sport"
                  onChange={this.handleChange}
                >
                  <option value>Open this select menu</option>
                  <option value="Football">Football</option>
                  <option value="Mens Basketball">Mens Basketball</option>
                  <option value="Womens Basketball">Womens Basketball</option>
                  <option value="Mens Soccer">Mens Soccer</option>
                  <option value="Womens Soccer">Womens Soccer</option>
                  <option value="Baseball">Baseball</option>
                  <option value="Softball">Softball</option>
                  <option value="Track and Field">Track and Field</option>
                </select>
              </div>
              <div className="form-group col-lg-7">
                <label for="opponent">Opponent:</label>
                <input
                  type="text"
                  className="form-control"
                  name="opponent"
                  defaultValue={this.props.opponent}
                  onChange={event => this.handleChange(event)}
                />
              </div>
            </div>
            <div className="row">
              <div className="form-group col-md-2">
                <div className="form-group col-xs-push-4 ">
                  <label>Date</label>
                  <SingleDatePicker
                    date={this.state.date} // momentPropTypes.momentObj or null
                    isOutsideRange={() => false}
                    onDateChange={date => this.setState({ date })} // PropTypes.func.isRequired
                    focused={this.state.focused} // PropTypes.bool
                    onFocusChange={({ focused }) => this.setState({ focused })} // PropTypes.func.isRequired
                    id="selectedDate" // PropTypes.string.isRequired,
                  />
                </div>
                <div className="form-group col-xs-2">
                  <label>Time</label>
                  <TimePicker
                    disableClock={true}
                    time={this.state.time}
                    onChange={time => this.setState({ time })}
                    value={this.state.time}
                    name="time"
                  />
                </div>
              </div>
            </div>
            <div className="input-group input-group-sm">
              <div className="custom-file">
                <FileBase64 multiple={true} onDone={this.getFiles.bind(this)} />
              </div>
            </div>
            <div className="row p-3">
              <div className="form-group text-black">
                <label for="details">
                  Please Enter Event Details. One Per Line.
                </label>
                <Editor
                  editorState={this.state.editorState}
                  wrapperClassName="demo-wrapper"
                  editorClassName="demo-editor bg-white"
                  onEditorStateChange={this.onEditorStateChange}
                  onContentStateChange={this.onContentStateChange}
                />
              </div>
            </div>
          </form>
        </div>
        <div className="card-footer">
          <button
            type="button"
            className="btn btn-secondary"
            data-dismiss="modal"
            onClick={e => {
              if (window.confirm("Are you sure you wish to delete this item?"))
                this.deleteItem(e);
            }}
          >
            Delete Promo
          </button>

          <button
            type="button"
            className="btn btn-primary"
            onClick={this.handleSubmit}
          >
            Save changes
          </button>
        </div>
      </div>
    );
  }
}
